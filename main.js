var menu = document.querySelector('#menu');

var openMenuClick = function (event) {
    document.getElementById('menuIcon').style.display = "none";
    document.getElementById('navBar').style.display = "block";
    document.querySelector("#menu p").style.display = "block";
    menu.removeEventListener('click', openMenuClick);
    menu.addEventListener('click', closeMenuClick);
};

var closeMenuClick = function(event){
    document.getElementById('menuIcon').style.display = "block";
    document.getElementById('navBar').style.display = "none";
    document.querySelector("#menu p").style.display = "none";
    menu.removeEventListener('click', closeMenuClick);
    menu.addEventListener('click', openMenuClick);
};

menu.addEventListener('click', openMenuClick);

var homeClick = function (event) { $( "#body" ).load("ajax/home.html"); };
var eduClick = function (event) { $( "#body" ).load("ajax/edu.html"); };
var projClick = function (event) { $( "#body" ).load("ajax/proj.html"); };
var skilClick = function (event) { $( "#body" ).load("ajax/skil.html"); };
var contClick =  function (event) { $( "#body" ).load("ajax/cont.html"); };

document.getElementById("home").addEventListener('click', homeClick);
document.getElementById("education").addEventListener('click', eduClick);
document.getElementById("projects").addEventListener('click', projClick);
document.getElementById("skills").addEventListener('click', skilClick);
document.getElementById("contact").addEventListener('click', contClick);

homeClick();
openMenuClick();
